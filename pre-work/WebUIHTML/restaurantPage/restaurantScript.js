function validateContact() {
	var incomplete
	var name = document.getElementById("name").value;
	var email = document.getElementById("email").value;
	var phone = document.getElementById("phone").value;
	var reason = document.getElementById("reason").value;
	var moreInfo = document.getElementById("expound").value;
	var bestDay = null;
	var day = document.getElementsByName("dayContact");
	for (var i=0; day[i]; ++i) {
		if(day[i].checked) {
			bestDay = day[i].value;
		}
	}

	if (name == "") {
		missingName.innerHTML = "We need to know your name!";
		incomplete = true;
	}

	if ((email == "") && (phone == "")) {
		missingContact.innerHTML = "We need to know at least one way to contact you!";
		incomplete = true;
	}

	if (reason == "other" && moreInfo == "") {
		missingExpound.innerHTML = "You've got to give us some additional information";
		incomplete = true;
	}

	if (bestDay == null) {
		missingDay.innerHTML = "We need to know what day to contact you";
		incomplete = true;
	}

	if (incomplete == true) {
		return false;
	}
}