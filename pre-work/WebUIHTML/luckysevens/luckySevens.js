function playGame() {
	hideResults();
	var showRolls = document.getElementById("alertActive").checked;
	var startingBet = document.getElementById("bet").value;
	var totalMoney = document.getElementById("bet").value;
		var totalMoney = parseInt(totalMoney);
	var maxMoney = totalMoney;
	var totalRolls = 0;
	var highRoll = 0;
	if (totalMoney < 1) {
		alert("Your starting bet must be at least $1.00!");
	}
	else {
		alert("Your starting bet is $" + totalMoney);
	}
	
	do {
		var totalRolls = ++totalRolls;
		var firstRoll = getRandomInt(1, 6);
		var secondRoll = getRandomInt(1, 6);
		var totalRoll = firstRoll + secondRoll;
		
		if (totalRoll == 7) {
			var totalMoney = totalMoney + 4;
			if (showRolls == true) {
				alert("You rolled 7! You win $4! Total money is $" + totalMoney +". You have rolled " + totalRolls + " times!");
			}
			if (totalMoney > maxMoney) {
				var maxMoney = totalMoney;
				var highRoll = totalRolls;
			}
		}
		else {
			var totalMoney = totalMoney - 1;
			if (showRolls == true) {
				alert("You rolled " + totalRoll + "! Sorry, you lose $1! Total money remaining is $" + totalMoney + ". You have rolled " + totalRolls + " times!");
			}
			if (totalMoney > maxMoney) {
				var maxMoney = totalMoney;
				var highRoll = totalRolls;
			}
		}
	} while (totalMoney >= 1);

	if (highRoll != 0) {
		if (showRolls == true) {
			alert("Game over! You rolled " + totalRolls + " times. The most money you had was $" + maxMoney + ". The roll count at the highest amount won was " + highRoll + ". Please play again!");
		}	
	}
	else {
		if (showRolls == true) {
			alert("Game over! You rolled " + totalRolls + " times. The most money you had was $" + maxMoney + ", which was before you even started playing! Please play again!");
		}
	}	

	showResults();
	beginBet.innerHTML = "$" + startingBet;
	totalAmountRolls.innerHTML = totalRolls;
	highestWon.innerHTML = "$" + maxMoney;
	if (highRoll == 0) {
		highRollCount.innerHTML = "You had more money before you started playing!";
	} else {
		highRollCount.innerHTML = highRoll;
	}
	playButton.innerHTML = "Play Again";
}

function getRandomInt(min, max) {
   return Math.floor(Math.random() * (max - min + 1) + min);
}

function doLoadStuff() {
	hideResults();
	hideInstructions();
}

function hideResults() {
	var divResults = document.getElementById("results");
	divResults.style.display = "none";
}

function showResults() {
	var divResults = document.getElementById("results");
	divResults.style.display = "block";
}

function hideInstructions() {
	var divInstructions = document.getElementById("instructions");
	divInstructions.style.display = "none";
	var linkIntructions = document.getElementById("clickInstructions");
	linkIntructions.style.display = "block";
}

function showInstructions() {
	var divInstructions = document.getElementById("instructions");
	divInstructions.style.display = "block";
	var linkIntructions = document.getElementById("clickInstructions");
	linkIntructions.style.display = "none";
}